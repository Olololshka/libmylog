Библиотека простого журнала для программ на Qt

Требуется:
    * >=Qt-4.8.0
    * >=cmake-4.8
	* libsettingscmdline (https://bitbucket.org/Olololshka/libsettingscmdline.git)
    * >=doxygen-1.8 (Документация)
    * >=git-1.7 (Получение исходных кодов)

Сборка:
    * Скачать исхохный код при помощи git
    $ git clone git clone https://bitbucket.org/Olololshka/libmylog.git

    * Создаем каталог для сборки
    $ mkdir libmylog/build && cd libmylog/build

    * запуск cmake
    ** для windows:
            $ cmake .. -G"MSYS Makefiles"
    ** для Linux:
            $ cmake ..
      В процессе могут возниукнуть ошибки, по причине нехватки некоторых обязательных компанентов.
      Воспользуйтесь документацией чтобы устранить эти ошибки.

    * Сборка
    make

    * Установка (для Linux требуются права администратора).
      Каталог установки по-умолчанию /usr/local, что соответствует
      <каталог установки MinGW>/msys/1.0/local в Windows
    # make install

    * Сборка документации
    $ make doc


Использование:
    В простейшем случае требуется один раз создать экземпляр класса MyLog::myLog и затем можно будет использовать
    макросы записи LOG_*(msg).

    Например:

    #include <mylog/mylog.h>

    ...

    MyLog::myLog log; // имя обьекта не важно, используется глобалный указатель MyLog::LOG

    ...

    LOG_INFO("Информационное сообщение.");

    if (error != 0)
    {
    LOG_ERROR("Упс. Ошибочка вышла.");
    }
